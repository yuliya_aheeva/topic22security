package by.training.homework20.services;

import by.training.homework20.daos.OrderDao;
import by.training.homework20.entities.Order;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Getter
public class OrderService {
    @Autowired
    private OrderDao orderDao;

    public Order createNewOrder(final Long userId) {
        Order order = new Order();
        order.setUserId(userId);
        orderDao.add(order);
        return order;
    }
}
