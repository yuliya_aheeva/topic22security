package by.training.homework20.services.security;

import by.training.homework20.daos.UserDao;
import by.training.homework20.entities.user.CustomUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserDao userDao;

    public UserDetailsServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userDao.findUserByName(s)
                .map(CustomUserDetails::new)
                .orElseThrow(() -> new RuntimeException("UserNotFound with name " + s));

    }
}

