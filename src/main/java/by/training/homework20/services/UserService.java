package by.training.homework20.services;

import by.training.homework20.daos.UserDao;
import by.training.homework20.entities.user.User;
import by.training.homework20.entities.user.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public User registerUser(final String userName, final String userPassword) {
        User user = new User();
        user.setName(userName);
        user.setPassword(userPassword);
        userDao.add(user);
        return user;
    }
}
