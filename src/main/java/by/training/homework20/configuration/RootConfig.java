package by.training.homework20.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = {"by.training.homework20.services.security", "by.training.homework20.repositories"})
public class RootConfig {

}
