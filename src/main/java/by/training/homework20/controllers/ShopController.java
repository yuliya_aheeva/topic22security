package by.training.homework20.controllers;

import by.training.homework20.services.ShopService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ShopController {
    private final ShopService shopService;

    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping("/")
    public String doGetWelcomePage() {
        return "welcome";
    }

    @PostMapping("/register")
    public String doPostWelcomePage(HttpServletRequest request) {
        if (request.getParameter("userName") != null && request.getParameter("userName").length() >= 1
                && request.getParameter("userPassword") != null && request.getParameter("userPassword").length() >= 1) {
            shopService.registerUser(request);
            if (request.getParameter("button") != null) {
                if (request.getParameter("consent") != null) {
                    request.getSession().setAttribute("consent", "on");
                    return "redirect:login";
                } else {
                    return "notAgree";
                }
            }
        }
        return "notFound";
    }

    @GetMapping("/catalog")
    public String doGetCatalogPage() {
        return "catalog";
    }

    @PostMapping("/catalog")
    public String doPostCatalogPage(HttpServletRequest request) {
        if (request.getParameter("submit") != null) {
            shopService.submitOrder(request);
            return "redirect:order";
        } else {
            shopService.addProductToList(request);
            return "redirect:catalog";
        }
    }

    @GetMapping("/preorder")
    public String doGetPreorderPage(HttpServletRequest request) {
        shopService.setAttributes(request);
        return "redirect:catalog";
    }

    @GetMapping("/order")
    public String doGetOrderPage() {
        return "order";
    }
}
