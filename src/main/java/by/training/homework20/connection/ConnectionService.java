package by.training.homework20.connection;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConnectionService {
    private final FakeProductRepository fakeProductRepository;

    public void createConnection() {
        fakeProductRepository.createTables();
        fakeProductRepository.addProductsToTable();
    }
}
