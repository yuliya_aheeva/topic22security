package by.training.homework20.repositories;

import by.training.homework20.daos.UserDao;
import by.training.homework20.entities.user.User;
import by.training.homework20.connection.LocalConnectDb;
import by.training.homework20.entities.user.UserRole;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Optional;

import static java.util.Optional.of;

@Repository
public class UserRepository implements UserDao {

    @Override
    public void add(User user) {
        String password = user.getPassword();
        String sqlQuery = "INSERT INTO Users (name, password) VALUES (?, ?)";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     sqlQuery, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.addBatch();
            int affectedRows = preparedStatement.executeUpdate();
            assert (affectedRows > 0);
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            generatedKeys.next();
            long id = generatedKeys.getLong(1);
            user.setId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUserByName(String userName) {
        String sqlQuery = "SELECT id, name, password FROM Users WHERE name = ?";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();

            User user = new User();

            while (resultSet.next()) {
                user.setId(resultSet.getLong("id"));
                user.setName(resultSet.getString("name"));
                user.setPassword(resultSet.getString("password"));
            }
            preparedStatement.executeQuery();
            return user;
        } catch (SQLException e) {
            return null;
        }

    }

    @Override
    public Optional<User> findUserByName(String name) {
        User user = getUserByName(name);
        user.setUserRole(UserRole.USER);
        if (user == null) {
            return Optional.empty();
        } else {
            return Optional.of(user);
        }
    }

    @Override
    public boolean isUserExist(String userName) {

        boolean isUserExist = false;

        String sqlQuery = "SELECT id, name, password FROM Users WHERE name = ?";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                isUserExist = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isUserExist;
    }
}
