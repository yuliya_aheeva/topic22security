package by.training.homework20.repositories;

import by.training.homework20.entities.Product;
import by.training.homework20.connection.LocalConnectDb;
import by.training.homework20.daos.ProductDao;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductRepository implements ProductDao {

    @Override
    public void add(Product product) {

        String sqlQuery = "INSERT INTO Products (title, price) VALUES (?, ?)";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setString(1, product.getTitle());
            preparedStatement.setBigDecimal(2, product.getPrice());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Product> getAllProducts() {

        List<Product> productList = new ArrayList<>();

        String sqlQuery = "SELECT id, title, price FROM Products";

        try (Connection connection = LocalConnectDb.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                Product product = new Product();
                product.setId(resultSet.getLong("id"));
                product.setTitle(resultSet.getString("title"));
                product.setPrice(resultSet.getBigDecimal("price"));

                productList.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productList;
    }
}
