package by.training.homework20.entities.user;

import lombok.*;

import java.io.Serializable;
import java.util.StringJoiner;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 42L;

    private long id;
    private String name;
    private String password;
    private UserRole userRole;

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("name='" + name + "'")
                .add("password='" + password + "'")
                .add("userRole=" + userRole)
                .toString();
    }

}
