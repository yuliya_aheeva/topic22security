package by.training.homework20.entities.user;

public enum UserRole {
    USER, ADMIN
}
