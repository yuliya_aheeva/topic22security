package by.training.homework20.daos;

import by.training.homework20.entities.Order;

public interface OrderDao {

    void add(Order order);

    Order getByUserId(long userId);

    void updateTotalPrice(long userId);

}
