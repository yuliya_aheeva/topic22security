package by.training.homework20.daos;

import by.training.homework20.entities.Product;

import java.util.List;

public interface OrderProductDao {

    void add(long orderId, long productId);

    double getTotalPrice(long userId);

    List<Product> getChosenList(long userId);

}
