package by.training.homework20.daos;

import by.training.homework20.entities.user.User;

import java.util.Optional;

public interface UserDao {

    void add(User user);

    User getUserByName(String userName);

    Optional<User> findUserByName(String name);

    boolean isUserExist(String userName);
}
