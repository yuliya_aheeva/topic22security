package by.training.homework20.servlets;

import by.training.homework20.daos.OrderDao;
import by.training.homework20.daos.OrderProductDao;
import by.training.homework20.daos.ProductDao;
import by.training.homework20.daos.UserDao;
import by.training.homework20.entities.Order;
import by.training.homework20.entities.Product;
import by.training.homework20.entities.user.User;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class MainPageServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private OrderDao orderDao;
    @Mock
    private OrderProductDao orderProductDao;
    @Mock
    private ProductDao productDao;
    @Mock
    private UserDao userDao;

    @InjectMocks
    private User user;
    @InjectMocks
    private Order order;
    @InjectMocks
    private List<Product> productList = new ArrayList<>();

    private String[] products;

    @Before
    public void setUp() {
        products = new String[]{"product"};
    }

    @Test
    public void testMainServletServicePart(){
        when(userDao.getUserByName("user")).thenReturn(user);
        when(orderDao.getByUserId(21)).thenReturn(order);
        when(orderProductDao.getChosenList(21)).thenReturn(productList);
        lenient().when(session.getAttribute("userName")).thenReturn("user");
        when(productDao.getAllProducts()).thenReturn(productList);

        Assert.assertEquals(userDao.getUserByName("user"), user);
        Assert.assertEquals(orderDao.getByUserId(21), order);
        assertEquals(orderProductDao.getChosenList(21), productList);
        assertEquals(productDao.getAllProducts(), productList);

        verify(userDao, times(1)).getUserByName("user");
        verify(orderDao, times(1)).getByUserId(21);
        verify(orderProductDao, times(1)).getChosenList(21);
        verify(productDao, times(1)).getAllProducts();
    }

    @Test
    public void testMainServletGetPart() throws IOException {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        when(request.getParameter("userName")).thenReturn("qwe");
        lenient().when(response.getWriter()).thenReturn(printWriter);
        when(session.getAttribute("consent")).thenReturn("on");
        when(request.getSession()).thenReturn(session);

        assertEquals(session, request.getSession());
        assertEquals("on", session.getAttribute("consent"));
        assertEquals("qwe", request.getParameter("userName"));

        verify(request, never()).getParameterValues("product");
        verify(session, atLeastOnce()).getAttribute("consent");
        verify(request, times(1)).getSession();
        verify(request, atLeastOnce()).getParameter("userName");
    }

    @Test
    public void testMainServletPostPart() {
        when(request.getParameter("submit")).thenReturn("true");
        when(request.getParameter("addItem")).thenReturn("true");
        when(request.getParameterValues("product")).thenReturn(products);
        when(request.getParameter("userName")).thenReturn("qwe");
        when(orderProductDao.getTotalPrice(21)).thenReturn((double) 54);

        assertEquals("true", request.getParameter("submit"));
        assertEquals("true", request.getParameter("addItem"));
        String[] expectedProductsArray = request.getParameterValues("product");
        assertArrayEquals(expectedProductsArray, products);
        assertEquals("qwe", request.getParameter("userName"));
        assertEquals(orderProductDao.getTotalPrice(21),  54.0);

        verify(request, atLeastOnce()).getParameter("submit");
        verify(request, times(1)).getParameter("userName");
        verify(request, times(1)).getParameterValues("product");
        verify(orderProductDao,times(1)).getTotalPrice(21);
    }
}